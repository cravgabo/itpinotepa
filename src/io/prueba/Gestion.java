package io.prueba;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;

public class Gestion extends Activity {
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.gestion);
	}
	
    public void volver(View view) {
    	finish();
    }
    
    public void lanzarHome(View view) {
    	finish();
    	Intent i = new Intent(this, Home.class );
        startActivity(i);
    }
}
