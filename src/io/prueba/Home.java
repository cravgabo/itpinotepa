package io.prueba;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;

public class Home extends Activity {
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.home);
	}
	
    public void lanzarMensaje(View view) {
        Intent i = new Intent(this, Mensaje.class );
        startActivity(i);
    }
    
    public void lanzarOferta(View view) {
        Intent i = new Intent(this, Oferta.class );
        startActivity(i);
    }
    
    public void lanzarMision(View view) {
        Intent i = new Intent(this, Mision.class );
        startActivity(i);
    }
    
    public void lanzarDirectorio(View view) {
        Intent i = new Intent(this, Home.class );
        startActivity(i);
    }
    
    public void lanzarSii(View view) {
        Intent i = new Intent(this, Sii.class );
        startActivity(i);
    }
    
    public void lanzarLlamada(View view){
    	//Intent i = new Intent(Intent.ACTION_CALL,
    	Intent i = new Intent(Intent.ACTION_DIAL,
    			Uri.parse("tel:9545435287"));
    	startActivity(i);
    }
    
    public void lanzarFacebook(View view) {
    	String uri = "fb://profile/100010768158087";
    	Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
    	startActivity(intent);
    }
    
    public void lanzarWeb(View view) {
    	Intent i = new Intent(this, Web.class );
        startActivity(i);
    }
    
    public void lanzarMaps(View view) {
    	Uri gmmIntentUri = Uri.parse("google.navigation:q=Instituto Tecnologico de Pinotepa, Santiago Pinotepa Nacional");
    	Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
    	mapIntent.setPackage("com.google.android.apps.maps");
    	startActivity(mapIntent);
    }
}
