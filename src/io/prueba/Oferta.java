package io.prueba;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;

public class Oferta extends Activity {
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.oferta);
	}
	
    public void lanzarInformatica(View view) {
        Intent i = new Intent(this, Informatica.class );
        startActivity(i);
    }
    
    public void lanzarAdministracion(View view) {
        Intent i = new Intent(this, Administracion.class );
        startActivity(i);
    }
    
    public void lanzarAgronomia(View view) {
        Intent i = new Intent(this, Agronomia.class );
        startActivity(i);
    }
    
    public void lanzarGestion(View view) {
        Intent i = new Intent(this, Gestion.class );
        startActivity(i);
    }
    
    public void lanzarContador(View view) {
        Intent i = new Intent(this, Contador.class );
        startActivity(i);
    }
    
    public void lanzarIndustrial(View view) {
        Intent i = new Intent(this, Industrial.class );
        startActivity(i);
    }
    
    public void lanzarSistemas(View view) {
        Intent i = new Intent(this, Sistemas.class );
        startActivity(i);
    }
    
    public void volver(View view) {
    	finish();
    } 

}
